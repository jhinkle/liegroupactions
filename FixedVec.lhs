Intro
=====

We define fixed length vectors here.  This is based on this [email
thread](http://www.haskell.org/pipermail/haskell/2005-May/015815.html) by David
Menendez from May 2005.

We need GADTs

> {-#OPTIONS -XGADTs #-}

And we're also going to use phantom types:

> {-#OPTIONS -XEmptyDataDecls #-}

Declare which module we're writing here...

> module FixedVec where

Type Definitions
================

Let's get to it, defining a base type Zero (zero length vector)

> data Zero

and a custom Successor type

> data Succ n

The OP says these are "phantom types" so they don't need constructors.  This
means they'll never be instantiated directly (they have no constructors so they
can't be).  They are used in conjunction with other type constructors to create
new types.  In particular they allow us to encode the natural numbers _within
the Haskell type system_.  That's the first step toward being able to expressed
fixed _sizes_ within the type system.  Anyway, I'll follow Menendez and write
out a few explicit typedefs to use as shortcuts:

> type One = Succ Zero
> type Two = Succ One
> type Three = Succ Two
> type Four = Succ Three

Now we define the actual fixed length vector type

> data Vec n a where
>   Nil :: Vec Zero a
>   Cons :: a -> Vec n a -> Vec (Succ n) a

`Cons` does the usual cons thing, and lets us write a non-`Nil` `Vec` of length
`Succ n` as a combination of the head and tail.  By the way we can also define
the head and tail directly:

> vhead :: Vec (Succ n) a -> a
> vhead (Cons x xs) = x

> vtail :: Vec (Succ n) a -> Vec n a
> vtail (Cons x xs) = xs

Inner products
==============

Now we want to make this thing useful as a vector type.  For that, the main
thing we'll want first is to be able to write inner products.  I could write a
recursively defined inner product:

> inner' :: Num a => Vec n a -> Vec n a -> a
> inner' Nil Nil = 0
> inner' (Cons x xs) (Cons y ys) = (x * y) + (inner' xs ys)

But the OP adds a little more functionality by defining `zipWith` and `foldr`
then combining those for the inner product

> vzipWith :: (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c
> vzipWith f Nil Nil = Nil
> vzipWith f (Cons x xs) (Cons y ys) = Cons (f x y) (vzipWith f xs ys)

> vfoldr :: (a -> b -> b) -> b -> Vec n a -> b
> vfoldr _ z Nil = z
> vfoldr f z (Cons x xs) = f x $ vfoldr f z xs

A shorthand for sums is probably not a bad idea..

> vsum :: (Num a) => Vec n a -> a
> vsum = vfoldr (+) 0

Now I can just combine these trivially to get the inner product

> inner :: (Nat n, Num a) => Vec n a -> Vec n a -> a
> inner x y = vsum $ x * y

Between the two now I think `inner` is easier to read than the recursive
`inner'`.  This is not how Menendez wrote inner, and I had to add some more type
signatures to make this type check, but that's actually not a bad thing.  This
is pretty much as simple an inner product as you could write I think.

One more trivial shortcut to get the norm of a vector

> vnormsq :: (Nat n, Num a) => Vec n a -> a
> vnormsq x = inner x x
> vnorm :: (Nat n, Floating a) => Vec n a -> a
> vnorm = sqrt . vnormsq

The OP gives a perfunctory instance of `Functor` for our constructor `Vec`:

> instance Functor (Vec n) where
>   fmap f Nil = Nil
>   fmap f (Cons x xs) = Cons (f x) $ fmap f xs

This is just miming the instance for an ordinary list.

Conversion to/from list
==================

This is just in case we want to get a list out of our fixed size vector.  Right
now it's only used to `Show`.  The implementation is trivial:

> toList :: Vec n a -> [a]
> toList = vfoldr (:) []

Possible approaches to zero function
====================================

The OP discusses how to define a `vec` function (I don't like that name) which
returns a `Vec n a` with a single element repeated for each component.  This is
tricky because of how to coerce the output type (the input is only a single
scalar of type `a`).

He goes on to use this sort of cryptic method that I don't understand yet.
Apparently the key is described
[here](http://www.informatik.uni-bonn.de/~ralf/publications.html#P21).  The gist
is that we need to define a natural number type class, `Nat`:

> class Nat n where
>   natCase :: NatCase g => g n

And `NatCase` is another new class

> class NatCase g where
>   caseZero :: g Zero
>   caseSucc :: Nat n => g (Succ n)

And now each of the two constructors are made instances of `Nat`

> instance Nat Zero where natCase = caseZero
> instance Nat n => Nat (Succ n) where natCase = caseSucc

The upside is that `Nat` is a class that has an extra specifier, either
`caseZero` or `caseSucc`.  This is cool because it lets the size of the natural
number be checked *during the type-checking at compile time*.  This is really
leveraging the GADT business.

OK, so for our case, this lets us write the function `vec` which takes a single
number and creates a vector with that as every component.  I might generally
call this `fill`.  First we create a new typedef `MkVec`.  This has the look of
the GADT crap I've been talking about:

> newtype MkVec a n = MkVec { runMkVec :: a -> Vec n a }

Now we use this to define `vec`

> vec :: Nat n => a -> Vec n a
> vec = runMkVec natCase

This is pretty ugly if you ask me because we should be able to go straight at
vec I would imagine, but that would be some weird combination of GADT inside
function definitions.  Anyway, we can see also that a `MkVec` is an instance of
`NatCase` also

> instance NatCase (MkVec a) where
>   caseZero = MkVec (\x -> Nil)
>   caseSucc = MkVec (\x -> Cons x (vec x))

That last part is pretty cool.  It shows how the "return types" are driving the
type checking and dispatch of all this crap.

Printing and comparing vectors
==============================

We want to make `Vec` an instance of `Show` and `Eq` in the obvious ways:

> instance Show a => Show (Vec n a) where show = show . toList

> instance Eq a => Eq (Vec n a) where
>     Nil       == Nil       = True
>     Cons x xs == Cons y ys = x == y && xs == ys
>
>     -- alternately, x == y = vfoldr (&&) True (vzipWith (==) x y)

Arithmetic (Instance of `Num`)
==============================

Ok, we need to be able to add subtract, multiply, negate, etc.  We do this by
making `Vec` act like a `Num`.  NOTE: this will give all element-wise
operations.

> instance (Nat n, Num a) => Num (Vec n a) where
>   (+) = vzipWith (+)
>   (*) = vzipWith (*)
>   (-) = vzipWith (-)
>   negate = fmap negate
>   abs = fmap abs
>   signum = fmap signum
>   fromInteger = vec . fromInteger  -- repeat integer in each component

The `Matrix` type
=================

Now we can use our fixed length vectors to create a fixed size matrix type.
This is pretty cool since we can actually now guarantee that the matrix is
actually rectangular and with the given size.  The definition is what you'd
expect, just a typedef to a compound type

> type Matrix m n a = Vec m (Vec n a)

So it's really just an `m`-length vector of `n`-length vectors of type `a`.  Now
again we create a function that will give us a matrix with a single element
repeated at every component:

> mat :: (Nat m, Nat n) => a -> Matrix m n a
> mat x = vec $ vec x

Now let's be able to turn a vector into a _row vector_, meaning a `Matrix` with
a single row

> row :: Vec n a -> Matrix One n a
> row v = Cons v Nil

That was the easy one.  Now to create a column vector, we'll need to represent
that as a vec of 1-vectors:

> col :: Vec n a -> Matrix n One a
> col = fmap (\x -> Cons x Nil)

Here we see that using `fmap`, we'll just be creating a singleton `Vec` for each
element of the input `Vec`.  This might be clearer if we just created a
singleton function...

Now as Menendez notes, this definition of matrix is nice because it's just a
specific type of `Vec`, which means we automatically have instances for `Eq` and
`Show`, as well as `Num`, meaning we can compare, show, and add/multiply
matrices.  What we haven't yet defined are the transpose and matrix multiply
functions.

Check out how we can transpose a matrix:

> transpose :: Nat n => Matrix m n a -> Matrix n m a
> transpose Nil = vec Nil
> transpose (Cons x xs) = vzipWith Cons x (transpose xs)

The base case is clear, transpose of a 0-size matrix is trivial.  Now what is
the induction step doing?  It's zipping the first row with the transpose of the
tail, using `Cons`.  This seems cryptic right?  But just imagine you want to
define a row in the transpose.  What do you do?  You go to the corresponding
element in the first row of input and start `Cons`ing til you get to the bottom.

Now finally we also define matrix-vector product:

> mult :: (Nat n, Nat m, Num a) => Matrix k m a -> Matrix m n a -> Matrix k n a
> mult x y = fmap (\r -> fmap (inner r) y') x
>   where y' = transpose y

Why these two `fmap`s?  Well the first is mapping an operation over rows.  The
rows in `x` determine the number of rows in the output.  Likewise the columns of
`y` are inner producted against each of those rows to produce the output matrix.
So this is doing exactly those two steps.

The last thing David Menendez blesses us with is an identity matrix, which of
course is square

> newtype IdMat a n = IdMat { runIdMat :: Matrix n n a }

> idMat :: (Nat n, Num a) => Matrix n n a
> idMat = runIdMat natCase
> instance Num a => NatCase (IdMat a) where
>     caseZero = IdMat Nil
>     caseSucc = IdMat (Cons (Cons 1 (vec 0)) (fmap (Cons 0) idMat))

Conversions from Lists
======================

Of course we'll want to use lists to set up vectors and matrices pretty often.
Let's define these things now.

Again, this is a little weird.  We have to create a new type (called `FromList`)
that is just a function that gives us (Maybe) a `Vec`.  This is exactly how we
implemented `vec`, except now we have the possibility of returning `Nothing`.
That would happen if we're asked to return a `Vec` that is bigger than the input
list.  In practice, that will be because we've coerced the type of a
computation, but at runtime the list given is not big enough.

So here's the typedef

> newtype FromList a n = FromList
>     { runFromList :: [a] -> Maybe (Vec n a) }

Then the function we actually call, `fromList`:

> fromList :: Nat n => [a] -> Maybe (Vec n a)
> fromList = runFromList natCase

Finally, the implementation.  This just handles the two cases and recurses on
the list if possible, or else returns `Nothing`.

> instance NatCase (FromList a) where
>     caseZero = FromList (\l -> Just Nil)
>     caseSucc = FromList (\l -> case l of
>                    x:xs -> fmap (Cons x) (fromList xs)
>                    []   -> Nothing
>                )

Now that we can convert a list to a `Maybe Vec`, we can easily extend this to
convert a list of lists to a matrix, assuming we give enough values in each row
and enough rows:

> fromLists :: (Nat m, Nat n) => [[a]] -> Maybe (Matrix m n a)
> fromLists xs = mapM fromList xs >>= fromList

Addons
======

Below are some things I have added myself.

Let's also provide a way to get a vector back from a row or column

> rowToVec :: Matrix One n a -> Vec n a
> rowToVec (Cons v Nil) = v
> colToVec :: Matrix n One a -> Vec n a
> colToVec = fmap toScalar

Here we've used the following, which just takes a 1-vector to its value

> toScalar :: Vec One a -> a
> toScalar (Cons x Nil) = x

Now we can easily multiply a matrix and vector to get another vector.

> vmult :: (Nat m, Num a) => Matrix k m a -> Vec m a -> Vec k a
> vmult r x = colToVec $ mult r $ col x

Also, outer product of two vectors to produce a matrix

> vouter :: (Nat m, Nat n, Num a) => Vec m a -> Vec n a -> Matrix m n a
> vouter x y = col x `mult` row y
